<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Atividade SASS</title>

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/main.css">
</head>
<body>

    <div class="container mt-5">
        <div class="row">
            <div class="col">
                <nav class="menuTopo">
                    <ul>
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Empresa</a></li>
                        <li><a href="#">Produtos</a></li>
                        <li><a href="#">Serviços</a></li>
                        <li><a href="#">Contatos</a></li>
                    </ul>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col text-center">
                <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Perferendis laborum aspernatur odit quidem soluta hic repellat enim quo. Officia nemo, aut iure enim optio iste labore vero repudiandae nihil deserunt!</p>
                <a href="#" class="botao-primario">Cadastrar</a>
                <a href="#" class="botao-segundario">Editar</a>
            </div>
        </div>
    </div>

        <script src="js/jquery.min.js"></script>
    	<script src="js/popper.min.js"></script>
    	<script src="js/bootstrap.min.js"></script>
    
</body>
</html>